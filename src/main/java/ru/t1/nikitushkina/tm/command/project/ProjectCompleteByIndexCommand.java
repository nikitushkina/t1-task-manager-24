package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @NotNull
    public static final String DESCRIPTION = "Complete project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        @Nullable final String userId = getUserId();
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

}
