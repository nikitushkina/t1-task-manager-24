package ru.t1.nikitushkina.tm.exception.entity;

public final class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}
